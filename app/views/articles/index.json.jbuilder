json.array!(@articles) do |article|
  json.extract! article, :price, :title, :description
  json.url article_url(article, format: :json)
end
