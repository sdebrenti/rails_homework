class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.decimal :price
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
